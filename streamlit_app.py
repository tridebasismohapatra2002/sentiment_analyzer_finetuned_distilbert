import streamlit as st
from response import classifier
from utils import *

st.markdown("<h1 style='text-align: center;'>Sentiment Analyzer</h1>", unsafe_allow_html=True)

st.markdown("<h3 style='text-align: center;'>Please provide the text and our system will classify it according to the sentiment it convey</h3>", unsafe_allow_html=True)

with st.form(key='sentiment_form'):
    text = st.text_input(label= 'text' , placeholder= 'Enter the text here....' , label_visibility = 'hidden')
    submit_button = st.form_submit_button(label='Submit')

if text and submit_button:

    with st.spinner('Classifying text....'):
        text = convert_lower(text)
        sentiment = classifier(text)
        emoji = selected_emoji(sentiment)

    st.markdown(f"<div style='text-align: center; font-size: 72px;'>{emoji}</div>", unsafe_allow_html=True)
    st.markdown(f"<h2 style='text-align: center;'>{sentiment.capitalize()}</h2>", unsafe_allow_html=True)