import tensorflow as tf
from transformers import TFDistilBertForSequenceClassification , DistilBertTokenizer

model = tf.keras.models.load_model('./best_model')

tokenizer = DistilBertTokenizer.from_pretrained('distilbert-base-uncased')

classes = ['sad' , 'joy' , 'love' , 'anger' , 'fear' , 'surprise']

def classifier(text):

    infr_encodings = tokenizer([text] , truncation = True , padding = True)

    inference_data = tf.data.Dataset.from_tensor_slices((
        dict(infr_encodings)
    )).batch(batch_size=1)

    predictions = model.predict(inference_data)

    logits = predictions['logits']

    probs = tf.nn.softmax(logits, axis=-1)

    class_id = tf.argmax(probs, axis=-1)

    return classes[class_id[0]]