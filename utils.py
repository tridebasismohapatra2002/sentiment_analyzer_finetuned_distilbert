import re
import emoji

sentiment_emoji = {
    'sad' : 'crying_face',
    'joy' : 'beaming_face_with_smiling_eyes' ,
    'love' : 'smiling_face_with_hearts',
    'anger' : 'face_with_steam_from_nose' ,
    'fear' : 'fearful_face' ,
    'surprise' : 'astonished_face'
}

def convert_lower(text):
    text = text.lower()
    return remove_special_char(text)

def remove_special_char(text):
    text = re.sub('[^a-zA-Z0-9]' , ' ' , text)
    text = re.sub('\s+' , ' ' , text)
    return text

def selected_emoji(sentiment):
    emotion = sentiment_emoji[sentiment]
    sticker = emoji.emojize(f':{emotion}:')
    return sticker

